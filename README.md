# Switch

The Switch game.

## You can play it using any JS engine

```javascript

// Load game options (see config.js)
var options = {...};

// Load game formulas (see formulas.js)
var formulas = {...}
var formulasInfo = {...}

// Load game previous state, if existent
var state = {...};

// Start the game
var game = new Switch(state);

// Player enter a word
var response = game.play('banana');

// The computer returns a number: 6
console.log(response);

// Save game state
... game.state();

// Player enter a second word
response = game.play('forbidden');

// The computer returns a number: 9
console.log(response);

// Player believes he got the formula. He wants to SWITCH
response = game.play('>');

// The computer now returns a word: 'motorcycle'
console.log(response);

// Save game state
... game.state();

// The player now tries to use the same formula the computer did.
response = game.play('10');

// The computer will let him know whether it is right or not
console.log(response);

```

## You can use the web page

```shell
gulp serve
```

## Backlog

### BETA
- ✅ Externalize configuration
- ✅ Save players stats locally
- ✅ Create command to reset player stats (RESET)
- ✅ Create confirmation for RESET command
- ✅ Replace character '!' for stats command with '$'
- ✅ Create tutorial
   - ✅ Define how intro message is used
   - ✅ Create narrative script describing messages from the initial level to advanced level.
      - ✅ For a certain type of message, there should be different versions, where the first ones are more verbose, didactic    
      - ✅ Script must present explanation of formulas
      - ✅ Message type versions should change along with game play.
      - ✅ Create tabular document to explicit rules and check version lifecycle for all types
   - ✅ Create Messages service
      - ✅ Returns a message based on message type
      - ✅ Receives game state as parameter to decorate/contextualize messages and define which message version to use for each type
      - ✅ Service must keep state of messages to define which message version to use for each
   - ✅ Allow formatting to messages (* _ \` \`\`\` \n): parse message before displaying
- ✅ Validate invalid number if player is guessing
- ✅ Minimize data that is sent to GA
- ✅ Handle end of levels
  - ✅ Do not increase levels
  - ✅ Notify the player
  - ✅ Allow the player to continue playing (pick a new formula, switch back)
  - ✅ Allow the player to keep learning formulas
  - ✅ Do not increase player hints
- ✅ Update STATS ($) command according to script
    - ✅ Use versions and formats
- ✅ Add total stats after stats per level
- ✅ Do not allow player to repeat words
    - ✅ Create record for player attempted words (per level)
    - ✅ Validate whether player is using already used word  
    - ✅ Create command to show used words (:)
- ✅ Add list of ratio (_corrects_ divided by _plays_) per level to STATS
    - ✅ Store _plays_ and _corrects_ by level
    - ✅ Store _words_ by level
- ✅ Create HELP command (?)
    - ✅ Shortly describes all commands (look at narrative script)
- ✅ Create the HINT command (!)
    - ✅ Formula should have description text
    - ✅ Describe one of the available formulas (cannot repeat a previous hinted formula)
    - ✅ Player starts with a certain number of HINT points and spends them each time command is ran
    - ✅ Level progress gives the player 1 more HINT point
    - ✅ Available HINT points should be added to STATS
    - ✅ After being promoted to a new level, 1 new HINT from the new level should be displayed  
- ✅ Create HINTS command (.)
    - ✅ Show all hints already shown to the player.
- ✅ Create ABOUT command (@)
   - ✅ Show game information:
      - ✅ Description of how to play and rules.
      - ✅ List of commands (similar to HELP commando)
      - ✅ Copyright, version, staff
- ✅ Enhance HTML UI
   - ✅ Input text margin
   - ✅ GO button removed
   - ✅ Responsive display of SWITCH button
   - ✅ Switch logo watermark on the background~

### BETA Feedback
- ✅ Font is too large for phones: some messages require lots of scrolling
- ✅ Players think Switch button is a "enter" button
    - ✅ Don't show button at the beginning
    - ✅ Introduce the button on first appearance
    - ✅ Hide button if the user use the input field  
    - ✅ Show the button again if the user clears the input field  

### Chat Version
- ✅ Internalize support for reset command (#)
- ✅ Standardize game object instantiation
- It will be hard to use < or > on the chat mobile app  

### Release 1
- Badges for formulas that were solved by the player
- Support formulas.js and words.js add-ins after release  
