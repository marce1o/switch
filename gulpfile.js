var concat = require('gulp-concat');
var gulp = require('gulp');
var bs = require('browser-sync').create(); // create a browser sync instance.

gulp.task('serve', function() {
  bs.init({
    server: {
      baseDir: "./site"
    },
    files: 'site/*.html, site/js/*.js, site/css/*.css, site/*.svg'
  });
});

gulp.task('deploy', function() {
  return gulp
    .src(['./site/**'])
    .pipe(gulp.dest(process.env.PAPEL_GAMES_SITE + '/switch'));
});

gulp.task('unify', function() {
  return gulp
    .src(['./site/js/config.js', './site/js/words.js', './site/js/formulas.js', './site/js/Messages.js', './site/js/Switch.js'])
    .pipe(concat('switch-all-in-one.js'))
    .pipe(gulp.dest('./dist'));
});
