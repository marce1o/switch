var fs = require('fs');

eval(fs.readFileSync('site/js/Switch.js') + ''); // Some common functions
eval(fs.readFileSync('site/js/Messages.js') + '');

var FORMULAS = 51;

var formulasInfo = [];
for (var i = 0; i < FORMULAS; i++) {
  formulasInfo.push('##' + i);
}

var options = {
  version: 'UNIT_TEST',
  initial_chances: 1001,
  initial_hints: 1002,
  level_promotion: 1003,
  levels: []
};
for (var i = -1; i < FORMULAS; i = i + 3) {
  options.levels.push(i);
}

var game = {
  challengeWord: 'CHALLENGE_WORD',
  previousChallengeWord: 'PREVIOUS_CHALLENGE_WORD',
  previousChallengeNumber: 222,
  chances: 444,
  hints: 555,
  watching: true,
  currentInput: 'CURRENT_INPUT',
  lastNumberForInput: 666,
  knownFormulas: [0, 10],
  lastKnownFormula: 10,
  playedLevels: [{
      level: 1,
      plays: 15,
      corrects: 3
    },
    {
      level: 2,
      plays: 18,
      corrects: 3
    },
    {
      level: 3,
      plays: 21,
      corrects: 3
    },
    {
      level: 4,
      plays: 27,
      corrects: 3
    },
    {
      level: 5,
      plays: 30,
      corrects: 3
    },
    {
      level: 6,
      plays: 33,
      corrects: 3
    },
    {
      level: 7,
      plays: 42,
      corrects: 3
    },
    {
      level: 8,
      plays: 51,
      corrects: 3
    },
    {
      level: 9,
      plays: 60,
      corrects: 3
    },
    {
      level: 10,
      plays: 90,
      corrects: 3
    }
  ],
  usedWords: ['banana', 'apple', 'grape', 'nut'],
  formulaID: 30
};

var messages = new Messages({
  game,
  options,
  formulasInfo
});

var missingTexts = ['Guess what: you can ask for a _HINT_', 'You are watching me using my formula', 'Not yet.', 'Nope.', 'Nah.'];


check(messages.showWrongGuess());
check(messages.showWrongGuess());

for (var i = 0; i < 30; i++) {
  if (i > 15) {
    game.watching = false;
  }
  if (i == 4) {
    check(messages.showWrongGuess());
    check(messages.showWrongGuess());
  } else {
    check(messages.showAbout());
    check(messages.showAllHints());
    check(messages.showAllWords());
    check(messages.showHelp());
    check(messages.showHint());
    check(messages.showIntro());
    check(messages.showInvalidNumber());
    check(messages.showLevelUp());
    check(messages.showNoMoreChances());
    check(messages.showNoMoreHints());
    check(messages.showNoMoreLevels());
    check(messages.showNothingToLearn());
    check(messages.showNumberForWord());
    check(messages.showRepeatWord());
    check(messages.showRightGuess());
    check(messages.showStats());
    check(messages.showSwitch());
    check(messages.showSwitchBack());
    check(messages.showWrongGuess());
    check(messages.showWrongWord());
    check(messages.showConfirmReset());
    check(messages.showCancelReset());
    check(messages.showResetExecuted());
  }
}
check(messages.showWrongGuess());
check(messages.showWrongGuess());

if (missingTexts.length > 0) {
  throw 'Missing text == ' + missingTexts[0];
}

console.log('SUCCESS!!!');

function check(message) {
  console.log('-------------------');
  console.log(message);
  if (message.match(/undefined/)) {
    throw 'I found an \'undefined\' value!';
  } else if (message.match(/NaN/)) {
    throw 'I found an \'NaN\' value!';
  } else if (message == 'I don’t know what to say...') {
    throw 'No message match.'
  }

  var i = missingTexts.length
  while (i--) {
    if (message.includes(missingTexts[i])) {
      missingTexts.splice(i, 1);
    }
  }

}
