function Switch(initialState) {

  if (!options) {
    throw 'Global variable "options" not present. Are you loading "config.js"?'
  }

  if (!formulas) {
    throw 'Global variable "formulas" not present. Are you loading "formulas.js"?'
  }

  if (!formulasInfo) {
    throw 'Global variable "formulasInfo" not present. Are you loading "formulas.js"?'
  }

  // Constants
  this.SWITCH_1 = '<';
  this.SWITCH_2 = '>';

  // Initialize game level and status
  this._start = function(initialState) {
    if (!initialState) {
      this._state = {
        challengeWord: '',
        previousChallengeWord: '',
        previousChallengeNumber: -1,
        chances: options.initial_chances,
        hints: options.initial_hints,
        watching: true, // if false, mode = 'guessing'
        lastInput: undefined,
        lastNumberForInput: -1,
        knownFormulas: [],
        lastKnownFormula: -1,
        playedLevels: [{
          level: 1,
          plays: 0,
          corrects: 0
        }],
        usedWords: []
      };
      this._state.formulaID = this.pickFormula();
      this.learnFormula();
      this.learnFormula();
    } else {
      this._state = initialState;
    }
    this.messages = new Messages({
      game: this._state,
      options,
      formulasInfo
    });
  }

  // Compute a player's "move"
  this.play = function(input) {

    var lastInput = this._state.currentInput;

    this._state.currentInput = input;


    if (input == '#') {

      return this.messages.showConfirmReset();

    } else if (lastInput == '#') {

      if (input == 'yes') {
        this.reset();
        return this.messages.showResetExecuted();
      } else {
        return this.messages.showCancelReset();
      }

    } else if (input == '$') {

      return this.messages.showStats();

    } else if (input == '?') {

      return this.messages.showHelp();

    } else if (input == ':') {

      return this.messages.showAllWords();

    } else if (input == '@') {

      return this.messages.showAbout();

    } else if (input == '.') {

      return this.messages.showAllHints();

    } else if (input == '!') {

      if (this._state.hints > 0) {

        var learned = this.learnFormula();

        if (learned) {

          this._state.hints--;
          return this.messages.showHint();

        } else {

          return this.messages.showNothingToLearn();

        }

      } else {

        return this.messages.showNoMoreHints();

      }

    } else if (this._state.watching) {
      // Players is watching

      if (input == this.SWITCH_1 || input == this.SWITCH_2) {
        // Switch command

        this._state.watching = false;
        this._state.challengeWord = this.pickWord();
        return this.messages.showSwitch();

      } else {
        // User entered a word

        word = this.checkWord(input);

        if (word) {
          // If it is a valid word

          if (!this._state.usedWords.includes(word)) {
            // If the words was not used before

            this._state.usedWords.push(word);
            this._state.playedLevels.last().plays++;
            this._state.lastNumberForInput = this.calculate(word);

            return this.messages.showNumberForWord();

          } else {

            return this.messages.showRepeatWord();

          }

        } else {

          return this.messages.showWrongWord();

        }
      }

    } else if (!this._state.watching) {
      // Player is guessing

      if (input == this.SWITCH_1 || input == this.SWITCH_2) {
        // Switch back command

        this._state.watching = true;
        return this.messages.showSwitchBack();

      } else if (input.match(/^\d+$/) == null) {
        // Invalid number
        return this.messages.showInvalidNumber();

      } else if (this.calculate(this._state.challengeWord) == parseInt(input)) {
        // Right guess!

        this._state.playedLevels.last().corrects++;


        this._state.chances = options.initial_chances;

        this._state.challengeWord = '';
        this._state.watching = true;
        this._state.formulaID = this.pickFormula();

        if ((this._state.playedLevels.last().corrects % options.level_promotion) == 0) {
          // Level UP!

          if ((this._state.playedLevels.length + 1) == options.levels.length) {
            // If there are no more levels:
            this.learnFormula();
            return this.messages.showNoMoreLevels();
          } else {
            // If there are more levels:
            this._state.playedLevels.push({
              level: this._state.playedLevels.length + 1,
              plays: 0,
              corrects: 0
            });
            this.learnFormula();
            this._state.hints++;
            return this.messages.showLevelUp();
          }

        } else {

          return this.messages.showRightGuess();

        }

      } else {
        // Wrong guess

        this._state.playedLevels.last().plays++;
        this._state.chances--;
        this._state.previousChallengeNumber = this.calculate(this._state.challengeWord);
        this._state.previousChallengeWord = this._state.challengeWord;

        if (this._state.chances == 0) {
          this._state.watching = true;
          this._state.chances = options.initial_chances;
          this._state.challengeWord = '';
          this._state.formulaID = this.pickFormula();
          return this.messages.showNoMoreChances();
        } else {
          this._state.challengeWord = this.pickWord();
          return this.messages.showWrongGuess();
        }

      }

    }

  }

  this.intro = function() {
    return this.messages.showIntro();
  }

  this.pickFormula = function() {
    return getRandomIntInclusive(0, options.levels[this._state.playedLevels.length]);
  };

  this.calculate = function(word) {
    return formulas[this._state.formulaID](word);
  }

  this.checkWord = function(input) {
    if (!input) {
      return undefined;
    }
    word = input.toLowerCase().trim();
    if (WORDS.includes(word)) {
      return word;
    }
    return undefined;
  }

  this.pickWord = function() {
    return randomize(WORDS)
  }

  this.state = function() {
    return this._state;
  }

  this.reset = function() {
    this._start();
  }

  this.learnFormula = function() {
    var unknownFormulas = [];

    for (var i = 0; i <= options.levels[this._state.playedLevels.length]; i++) {
      if (!this._state.knownFormulas.includes(i)) {
        unknownFormulas.push(i);
      }
    }

    if (unknownFormulas.length > 0) {
      var formula = randomize(unknownFormulas);
      this._state.knownFormulas.push(formula);
      this._state.lastKnownFormula = formula;
      return true;
    } else {
      return false;
    }

  }

  this._start(initialState);

}

function randomize(items) {
  index = getRandomIntInclusive(0, items.length - 1);
  return items[index];
}

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/trunc
if (!Math.trunc) {
  Math.trunc = function(v) {
    return v < 0 ? Math.ceil(v) : Math.floor(v);
  };
}

// https://stackoverflow.com/questions/610406/javascript-equivalent-to-printf-string-format#4256130
String.prototype.format = function() {
  var formatted = this;
  for (var i = 0; i < arguments.length; i++) {
    var regexp = new RegExp('\\{' + i + '\\}', 'gi');
    formatted = formatted.replace(regexp, arguments[i]);
  }
  return formatted;
}

Array.prototype.last = function() {
  if (this.length == 0) {
    return undefined;
  }
  return this[this.length - 1];
};
