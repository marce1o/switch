var options = {
  // Game version
  version: 'BETA.1',
  // Chances to guess a new formula, before computer change it
  initial_chances: 3, // 3
  // Initial hint points (used for the command HINT)
  initial_hints: 1, // 1
  // Number of correct guesses to clear the level
  level_promotion: 5, // 5
  // Maps level number to the maximum index of formula to that level (first level is 1 and must have at least 2 formulas; first formula is 0)
  levels: [0, 3, 7, 11, 14, 17, 20, 23, 26, 29, 32, 35, 38, 41, 44, 47, 50, 53, 56, 59, 62, 66] // [0, 3, 7, 11, 14, 17, 20, 23, 26, 29, 32, 35, 38, 41, 44, 47, 50, 53, 56, 59, 62, 66] = 21 levels
}
