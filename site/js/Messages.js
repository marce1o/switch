function Messages(context) {

  this.MSG_ABOUT = 'about';
  this.MSG_ALL_HINTS = 'all_hints';
  this.MSG_ALL_WORDS = 'all_words';
  this.MSG_HELP = 'help';
  this.MSG_HINT = 'hint';
  this.MSG_INTRO = 'intro';
  this.MSG_INVALID_NUMBER = 'invalid_number';
  this.MSG_LEVEL_UP = 'level_up';
  this.MSG_NO_MORE_CHANCES = 'no_more_chances';
  this.MSG_NO_MORE_HINTS = 'no_more_hints';
  this.MSG_NO_MORE_LEVELS = 'no_more_levels';
  this.MSG_NOTHING_TO_LEARN = 'nothing_to_learn';
  this.MSG_NUMBER_FOR_WORD = 'number_for_word';
  this.MSG_REPEAT_WORD = 'repeat_word';
  this.MSG_RIGHT_GUESS = 'right_guess';
  this.MSG_STATS = 'stats';
  this.MSG_SWITCH = 'switch';
  this.MSG_SWITCH_BACK = 'switch_back';
  this.MSG_WRONG_GUESS = 'wrong_guess';
  this.MSG_WRONG_WORD = 'wrong_word';
  this.MSG_CONFIRM_RESET = 'confirm_reset';
  this.MSG_RESET_EXECUTED = 'reset_executed';
  this.MSG_CANCEL_RESET =  'cancel_reset';

  if (!context.game.messages) {
    context.game.messages = {};
  }

  this._show = function(type) {

    if (!context.game.messages[type]) {
      context.game.messages[type] = 0;
    }

    var message = this._parse(this.getMessage(type), type);

    context.game.messages.lastType = type;
    context.game.messages[type] = context.game.messages[type] + 1;

    return message;
  }

  this.getMessage = function(type) {

    var message = 'I don’t know what to say...';
    var times = context.game.messages[type];
    var lastMessageType = context.game.messages.lastType;

    if (type == this.MSG_ABOUT) {
      message = '\
About *Switch*\n\n\
This is a word puzzle game where the player has to identify which formula the computer picked - a formula is an algorithm, a rule that can be used to calculate a number out of a word.\n\n\
First the computer chooses one formula. Then the player enters words so the computer uses the formula to calculate a number\
 - this is the watching mode. The player keeps entering words until he or she identifies the formula.\n\
When ready, the player switches to the guessing mode: now the computer chooses a word so the player can use the formula he or she believes the computer was using before. \
The player then enter a number. If the player guess is right, the computer picks a new formula and switches back to the watching mode. \
If the number is not correct, the player still has some chances to try again. It is also possible to switch back to watching mode anytime.\n\
As the player correctly guesses computer formulas, new levels are open and new formulas are learned by the computer.\n\n\
_version:_ *{context.options.version}*\n\
_developer:_ *marcelo@papel.games*\n\
_publisher:_ *papel.games*\n\n\
You can type *?* anytime to get more help.\n\n\
_Copyright {new Date().getFullYear()} Papel Games_\n\n\
Have fun!';
    } else if (type == this.MSG_CONFIRM_RESET) {
      message = 'Are you *SURE* you want to reset your history?\n\nThis will *PERMANENTELLY* erase *ALL* your game progress.\n\nEnter _yes_ to confirm or *anything else* to cancel this.';
    } else if (type == this.MSG_RESET_EXECUTED) {
      message = 'Your history has been reset.';
    } else if (type == this.MSG_CANCEL_RESET) {
      message = 'I thought so...';
    } else if (type == this.MSG_ALL_WORDS) {
      message = 'These are all the *{context.game.usedWords.length}* valid words you already entered: ';
      if (context.game.usedWords.length > 0) {
        message += '_{context.game.usedWords.join(", ")}_';
      }
    } else if (type == this.MSG_ALL_HINTS) {
      message = 'These are the *{context.game.knownFormulas.length}* formulas you learned so far:\n\n';
      for (var i = 0; i < context.game.knownFormulas.length; i++) {
        message += '*' + (i + 1) + ' - *' + context.formulasInfo[context.game.knownFormulas[i]] + '\n\n'
      }
    } else if (type == this.MSG_HELP) {
      if (context.game.watching) {
        message = 'You can type:\n\n';
        message += ' *any word* to watch me using my formula to transform it into a number\n\n';
        message += ' *>* or *<* to switch to the mode where you guess my formula by entering a number\n\n'
      } else {
        message = 'You can type:\n\n';
        message += ' *any number* calculated using my formula on my word: *{context.game.challengeWord}*\n\n';
        message += ' *>* or *<* to switch back to the mode where you watch me running my formula on your words\n\n'
      }
      message += ' *?* to see this help\n\n';
      message += ' *:* to see all the words you have used\n\n';
      message += ' *$* to see your current status and stats \n\n';
      message += ' *#* to reset your stats and used words\n\n';
      message += ' *!* to see a new formula hint\n\n';
      message += ' *.* to see previous formula hints\n\n';
      message += ' *@* for more on gameplay and credits';
      context.game.messages.switchButtonRevealed = true;
    } else if (type == this.MSG_HINT) {
      if (times == 0) {
        message = 'When you ask for a hint, I will introduce you to one formula that I know and you still don’t know.\n\nRemember: each level you pass I learn new formulas.\n\nYou can use this command a limited amount of times: now you have *{context.game.hints}* left. \n\nBut don’t worry: each level you pass I also increase this amount. Use it wisely!\n\nHere’s a new formula:\n\n{context.formulasInfo[context.game.lastKnownFormula]}'
      } else {
        message = 'You have *{context.game.hints}* more hint(s). Here’s one of my formulas:\n\n{context.formulasInfo[context.game.lastKnownFormula]}'
      }
    } else if (type == this.MSG_INTRO) {
      if (times == 0) {
        message = 'Welcome to *Switch*! _({context.options.version})_\n\nSwitch is a word puzzle game, where you have to figure out how I transform a word into a number.\n\nFirst you have to enter a word. Go ahead, type any existent English word and hit return.'
      } else {
        message = 'Welcome back to *Switch*. \n\n_You can type_ *?* _anytime to get some help._\n\n' + this.getMessage(this.MSG_STATS);
      }
    } else if (type == this.MSG_INVALID_NUMBER) {
      message = 'Invalid number. I am waiting for a number for *{context.game.challengeWord}*\n\nYou can type *?* anytime to get some help.'
    } else if (type == this.MSG_LEVEL_UP) {
      if (times == 0) {
        message = 'Well done! You entered level *{context.game.playedLevels.length}*.\n\nI learned *{context.options.levels[context.game.playedLevels.length] - context.options.levels[context.game.playedLevels.length - 1]}* new formula(s), such as this one:\n\n{context.formulasInfo[context.game.lastKnownFormula]}\n\nI picked a new formula (from the *{context.options.levels[context.game.playedLevels.length] + 1}* available now). And remember I can repeat formulas. \n\nEnter a new word.'
      } else {
        message = 'Well done! You entered level *{context.game.playedLevels.length}*.\n\nI learned *{context.options.levels[context.game.playedLevels.length] - context.options.levels[context.game.playedLevels.length - 1]}* new formula(s), such as this one:\n\n{context.formulasInfo[context.game.lastKnownFormula]}\n\nI picked a new formula (from the *{context.options.levels[context.game.playedLevels.length] + 1}* available now). Enter a new word.'
      }
    } else if (type == this.MSG_NO_MORE_CHANCES) {
      message = 'I am sorry. For the word *{context.game.previousChallengeWord}* the number would be *{context.game.previousChallengeNumber}*.\n\nYou used your *{context.options.initial_chances}* chances to guess the formula. I will have to switch back.\n\nI picked a _new_ formula (yes, I can repeat formulas). Enter a new word and hit return.'
    } else if (type == this.MSG_NO_MORE_HINTS) {
      if (times == 0) {
        message = 'Bummer... you have no more hint points to use. Clear this level and I will give you one more.';
      } else {
        message = 'You ran out of hints. Good luck!';
      }
    } else if (type == this.MSG_NO_MORE_LEVELS) {
      if (times == 0) {
        message = 'Well done! You cleared this level!\n\nCurrently I have no more levels for you, but I bet there are formulas you did not experienced yet. Keep playing to increase your score and learn more formulas, such as this one:\n\n{context.formulasInfo[context.game.lastKnownFormula]}\n\n*New levels are coming soon!*\n\nThank you for playing, by the way!\n\nI picked a new formula. Enter a new word.';
      } else {
        message = 'Well done! You cleared this level again!\n\nI still don’t have new levels for you, but you can keep playing this level to increase your score and learn more formulas, such as this one:\n\n{context.formulasInfo[context.game.lastKnownFormula]}\n\n*New levels are coming soon!*\n\nI picked a new formula. Enter a new word.';
      }
    } else if (type == this.MSG_NOTHING_TO_LEARN) {
      message = 'You know all the *{context.game.knownFormulas.length}* formulas I know! If you want to see all the formulas you learned, type *.*';
    } else if (type == this.MSG_NUMBER_FOR_WORD) {
      if (times == 0) {
        message = 'Great choice. Now I will choose a formula to transform that word into a number.\n\nFor example, {context.formulasInfo[context.game.knownFormulas[0]]}\n\nOr maybe {context.formulasInfo[context.game.knownFormulas[1]]}\n\nCurrently I know *{context.options.levels[context.game.playedLevels.length] + 1}* formulas. I picked one formula. And applying it to your word I have:\n\n\t_{context.game.currentInput}_ = *{context.game.lastNumberForInput}*\n\nNow try another word. You cannot use the same word twice!'
      } else if (lastMessageType == this.MSG_NUMBER_FOR_WORD && times == 1) {
        message = 'Right. Using the _same_ formula I picked before, now I have:\n\n\t_{context.game.currentInput}_ = *{context.game.lastNumberForInput}*\n\nWe are getting there. Enter one more word and hit enter.'
      } else if (times == 1) {
        message = 'Right. Using the formula I picked, now I have:\n\n\t_{context.game.currentInput}_ = *{context.game.lastNumberForInput}*\n\nTry another word.'
      } else if (times == 2) {
        message = 'OK. Using the formula, now I have:\n\n\t_{context.game.currentInput}_ = *{context.game.lastNumberForInput}*\n\nIf you think you got the formula, you can switch: type *>* or *<*\nOtherwise, try another word.'
        context.game.messages.switchButtonRevealed = true;
      } else if (times < 6) {
        message = 'The result is:\n\n\t_{context.game.currentInput}_ = *{context.game.lastNumberForInput}*\n\nYou can either switch (type *>* or *<*) or enter another word.'
        context.game.messages.switchButtonRevealed = true;
      } else {
        message = '_{context.game.currentInput}_ = *{context.game.lastNumberForInput}*'
      }
    } else if (type == this.MSG_REPEAT_WORD) {
      if (times == 0) {
        message = 'You already used that word. Remember: you _cannot_ repeat words. \nTry another one.'
      } else {
        message = 'You cannot repeat words. A tip: type *:* anytime to check the words you already used. \nType a new word.'
      }
    } else if (type == this.MSG_RIGHT_GUESS) {
      if (times == 0) {
        message = 'Well done. I guess you got the formula right.\n\nDid I mention before that you are at level *{context.game.playedLevels.length}*?';
        var toNextLevel = context.options.level_promotion - context.game.playedLevels.last().corrects;
        if (toNextLevel > 0) {
          message += ' Yes, and with *' + toNextLevel + '* more right answer(s) you clear this level.';
        }
        message += '\n\nI picked a _new_ formula (I have *{context.options.levels[context.game.playedLevels.length] + 1}* available at this level). Enter a new word and hit return.'
      } else {
        message = 'Well done!'
        var toNextLevel = context.options.level_promotion - context.game.playedLevels.last().corrects;
        if (toNextLevel > 0) {
          message += ' You need *' + toNextLevel + '* more correct guess(es) to the next level.';
        }
        message += '\n\nI picked a _new_ formula. Enter a new word.'
      }
    } else if (type == this.MSG_STATS) {

      if (context.game.watching) {
        message = 'You are watching me using my formula. You need to enter a new word.';
      } else {
        message = 'You are trying to guess my formula by using it against my word *{context.game.challengeWord}*.';
      }

      message += '\n\nYour current level is *{context.game.playedLevels.length}*. At this level I know *{context.options.levels[context.game.playedLevels.length] + 1}* formulas.\nYou know *{context.game.knownFormulas.length}* formula(s). You can use *{context.game.hints}* more hint(s).\nYou have *{context.game.chances}* more chance(s) to guess this formula.';

      var toNextLevel = context.options.level_promotion - context.game.playedLevels.last().corrects;
      if (toNextLevel > 0) {
        message += '\nYou need *' + toNextLevel + '* more correct guess(es) to clear this level.';
      }

      message += '\n\nSo far you entered *{context.game.usedWords.length}* words and this is how you did each level:\n\n';
      message += '_level_\t_rate_\t_success/plays_\n';
      var totalCorrects = 0;
      var totalPlays = 0;
      for (stats of context.game.playedLevels) {
        totalPlays += stats.plays;
        totalCorrects += stats.corrects;
        var ratio = 0;
        if (stats.plays > 0) {
          ratio = Math.floor(stats.corrects / stats.plays * 100);
        }
        message += ' *' + stats.level + '*\t*' + ratio + '%*\t(' + stats.corrects + '/' + stats.plays + ')\n';
      }
      var totalRatio = 0;
      if (totalPlays > 0) {
        totalRatio = Math.floor(totalCorrects / totalPlays * 100);
      }
      message += 'TOTAL\t' + totalRatio + '%\t(' + totalCorrects + '/' + totalPlays + ')\n';

      if (context.game.watching) {
        message += '\nEnter a new word and hit return.';
      } else {
        message += '\nEnter the number for *{context.game.challengeWord}*';
      }

    } else if (type == this.MSG_SWITCH) {
      if (times == 0) {
        message = 'Now we switched positions: _I_ will give you a word and you have to transform it into a number.\n\nUsing my formula, what is the number for the word *{context.game.challengeWord}*?'
      } else if (times == 1) {
        message = 'Now I will give you a word and you give me a number.\n\nUsing my formula, what is the number for the word *{context.game.challengeWord}*?'
      } else if (times == 2) {
        message = 'Switching so I give you a word and you give me a number.\n\nWhat is the number for the word *{context.game.challengeWord}*?'
      } else {
        message = 'What is the number for the word *{context.game.challengeWord}*?'
      }
    } else if (type == this.MSG_SWITCH_BACK) {
      message = 'Switching back…\n\nI am still using the _same_ formula. Enter a new _word_ and hit return.'
    } else if (type == this.MSG_WRONG_GUESS) {
      if (times == 0) {
        message = 'Not exactly. For the word *{context.game.previousChallengeWord}* the number would be *{context.game.previousChallengeNumber}*.\n\nLet’s try a new word: *{context.game.challengeWord}*.'
      } else if (lastMessageType == this.MSG_WRONG_GUESS && times < 5) {
        message = 'Nah. For the word *{context.game.previousChallengeWord}* the number would be *{context.game.previousChallengeNumber}*.\n\nGuess what: you can ask for a _HINT_ by entering *!*\n\nLet’s try a new word: *{context.game.challengeWord}*.'
      } else if (lastMessageType == this.MSG_WRONG_GUESS && times > 5) {
        message = 'Not yet. *{context.game.previousChallengeWord}* = *{context.game.previousChallengeNumber}*.\n\nIf you think you still don’t know the formula, you can always switch back by typing *<* or *>*.\n\nLet’s try a new word: *{context.game.challengeWord}*.'
        context.game.messages.switchButtonRevealed = true;
      } else {
        message = 'Nope. *{context.game.previousChallengeWord}* = *{context.game.previousChallengeNumber}*. You have *{context.game.chances}* chance(s) left.\n\nLet’s try a new word: *{context.game.challengeWord}*.'
      }
    } else if (type == this.MSG_WRONG_WORD) {
      message = 'I am not sure that is an _English_ word - at least I don’t know it. Please try another one.'
    }
    return message;
  }

  this._parse = function(message, type) {

    regexp = /{[^}]+}/;
    let match = message.match(regexp);
    while (match != null) {

      var expression = message.substr(match.index + 1, match[0].length - 2);
      var evaluated = eval(expression);
      message = message.substring(0, match.index) + evaluated + message.substring(match.index + match[0].length);
      match = message.match(regexp);
    }
    return message;
  }

  this.showAbout = function() {
    return this._show(this.MSG_ABOUT);
  }
  this.showAllHints = function() {
    return this._show(this.MSG_ALL_HINTS);
  }
  this.showAllWords = function() {
    return this._show(this.MSG_ALL_WORDS);
  }
  this.showHelp = function() {
    return this._show(this.MSG_HELP);
  }
  this.showHint = function() {
    return this._show(this.MSG_HINT);
  }
  this.showIntro = function() {
    return this._show(this.MSG_INTRO);
  }
  this.showInvalidNumber = function() {
    return this._show(this.MSG_INVALID_NUMBER);
  }
  this.showLevelUp = function() {
    return this._show(this.MSG_LEVEL_UP);
  }
  this.showNoMoreChances = function() {
    return this._show(this.MSG_NO_MORE_CHANCES);
  }
  this.showNoMoreHints = function() {
    return this._show(this.MSG_NO_MORE_HINTS);
  }
  this.showNoMoreLevels = function() {
    return this._show(this.MSG_NO_MORE_LEVELS);
  }
  this.showNothingToLearn = function() {
    return this._show(this.MSG_NOTHING_TO_LEARN);
  }
  this.showNumberForWord = function() {
    return this._show(this.MSG_NUMBER_FOR_WORD);
  }
  this.showRepeatWord = function() {
    return this._show(this.MSG_REPEAT_WORD);
  }
  this.showRightGuess = function() {
    return this._show(this.MSG_RIGHT_GUESS);
  }
  this.showStats = function() {
    return this._show(this.MSG_STATS);
  }
  this.showSwitch = function() {
    return this._show(this.MSG_SWITCH);
  }
  this.showSwitchBack = function() {
    return this._show(this.MSG_SWITCH_BACK);
  }
  this.showWrongGuess = function() {
    return this._show(this.MSG_WRONG_GUESS);
  }
  this.showWrongWord = function() {
    return this._show(this.MSG_WRONG_WORD);
  }
  this.showConfirmReset = function() {
    return this._show(this.MSG_CONFIRM_RESET);
  }
  this.showResetExecuted = function() {
    return this._show(this.MSG_RESET_EXECUTED);
  }
  this.showCancelReset = function() {
    return this._show(this.MSG_CANCEL_RESET);
  }

}
