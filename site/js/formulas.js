const VOWELS = 'aeiou';
const CONSONANTS = 'bcdfghjklmnpqrstvwxyz';

// The formula inventory
var formulas = [];
var formulasInfo = [];
var sequoiaNumber = [];

// Word size (1)
formulasInfo.push('I can count the number of letters of the word.\n\n\t_car_ = *3*\n\t_metropolis_ = *10*');
formulas.push(function(word) {
  return word.length;
});
sequoiaNumber.push(7);

// Vowel position (5)
for (let vowel of VOWELS) {
  formulas.push(function(v) {
    return function(word) {
      return word.indexOf(v) + 1;
    };
  }(vowel));
}
const BASE_TEXT_1 = 'I can use the position of the first letter *@* in the word.\n\n';
formulasInfo.push(BASE_TEXT_1.replace(/@/, 'A') + '\t_c_*a*_r_ = *2*\n\t*a*_nagram_ = *1*');
sequoiaNumber.push(7);
formulasInfo.push(BASE_TEXT_1.replace(/@/, 'E') + '\t_mobil_*e* = *6*\n\t_b_*e*_ef_ = *2*');
sequoiaNumber.push(2);
formulasInfo.push(BASE_TEXT_1.replace(/@/, 'I') + '\t_mob_*i*_le_ = *4*\n\t_h_*i* = *2*');
sequoiaNumber.push(6);
formulasInfo.push(BASE_TEXT_1.replace(/@/, 'O') + '\t_m_*o*_bile_ = *2*\n\t_pian_*o* = *5*');
sequoiaNumber.push(5);
formulasInfo.push(BASE_TEXT_1.replace(/@/, 'U') + '\t_lang_*u*_age_ = *5*\n\t*u*_nbelievable_ = *1*');
sequoiaNumber.push(4);

// Reverse vowel position (5)
for (let vowel of VOWELS) {
  formulas.push(function(v) {
    return function(word) {
      index = word.lastIndexOf(v);
      if (index < 0) {
        return 0;
      }
      return word.length - index;
    };
  }(vowel));
}
const BASE_TEXT_2 = 'I can use the position of the first letter *@* in the word, _counting from the end_.\n\n';
formulasInfo.push(BASE_TEXT_2.replace(/@/, 'A') + '\t_c_*a*_r_ = *2*\n\t_anagr_*a*_m_ = *2*');
sequoiaNumber.push(1);
formulasInfo.push(BASE_TEXT_2.replace(/@/, 'E') + '\t_mobil_*e* = *1*\n\t_be_*e*_f_ = *2*');
sequoiaNumber.push(6);
formulasInfo.push(BASE_TEXT_2.replace(/@/, 'I') + '\t_mob_*i*_le_ = *3*\n\t_h_*i* = *1*');
sequoiaNumber.push(2);
formulasInfo.push(BASE_TEXT_2.replace(/@/, 'O') + '\t_m_*o*_bile_ = *5*\n\t_pian_*o* = *1*');
sequoiaNumber.push(3);
formulasInfo.push(BASE_TEXT_2.replace(/@/, 'U') + '\t_lang_*u*_age_ = *4*\n\t*u*_nbelievable_ = *12*');
sequoiaNumber.push(4);

// Number of vowels (1)
formulas.push(function(word) {
  var matches = word.match(RegExp('[' + VOWELS + ']', 'g'));
  if (!matches) {
    return 0;
  }
  return matches.length;
});
formulasInfo.push('I can count the number of vowels in the word.\n\n\t_c_*a*_r_ = *1*\n\t*a*_n_*a*_gr_*a*_m_ = *3*');
sequoiaNumber.push(5);

// Number of consonants (1)
formulas.push(function(word) {
  var matches = word.match(RegExp('[' + CONSONANTS + ']', 'g'));
  if (!matches) {
    return 0;
  }
  return matches.length;
});
formulasInfo.push('I can count the number of consonants in the word.\n\n\t*c*_a_*r* = *2*\n\t_a_*n*_a_*gr*_a_*m* = *4*');
sequoiaNumber.push(2);

// Position in the alphabet of the first letter (1)
formulas.push(function(word) {
  return word.charCodeAt(0) - 96;
});
formulasInfo.push('I can use the position in the alphabet of the first letter in the word.\n\n\t*c*_ar_ = *3*\n\t*u*_ncle_ = *21*');
sequoiaNumber.push(19);

// Number of the letters that are unique in the word (1)
formulas.push(function(word) {
  var unique = 0;
  for (letter of word) {
    if (word.match(RegExp(letter, 'g')).length == 1) {
      unique++;
    }
  }
  return unique;
});
formulasInfo.push('I can count the number of letters that do not repeat in the word.\n\n\t_banana_ = *1*\n\t_fig_ = *3*');
sequoiaNumber.push(7);

// Position in the alphabet of the last letter (1)
formulas.push(function(word) {
  return word.charCodeAt(word.length - 1) - 96;
});
formulasInfo.push('I can use the position in the alphabet of the _last_ letter in the word.\n\n\t_ca_*r* = *18*\n\t_uncl_*e* = *5*');
sequoiaNumber.push(1);

// Position in the alphabet of the letter that first shows up as double letters (1)
formulas.push(function(word) {
  var position = 0;
  match = word.match(/([a-z])\1/);
  if (match) {
      position = match[1].charCodeAt(0) - 96;
  }
  return position;
});
formulasInfo.push('I can use the position in the alphabet of the letter found in the first double letter pair of the word.\n\n\t_inte_*ll*_igent_ = *12*\n\t_a_*bb*_ey_ = *2*');
sequoiaNumber.push(0);

// Convert word to binary: vowels are 1 and consonants are 0.
formulas.push(function(word) {
  var converted = '';
  for (letter of word) {
    if (VOWELS.includes(letter)) {
      converted += '1';
    } else {
      converted += '0';
    }
  }
  return parseInt(converted);
});
formulasInfo.push('I can convert the word to a binary number: vowels become *1*s and consonants become *0*s.\n\n\t_inertia_ = *1010011*\n\t_flint_ = *00100*');
sequoiaNumber.push(101111);

// Sum of all the alphabet positions of all the letters
formulas.push(function(word) {
  var sum = 0;
  for (letter of word) {
    sum += (letter.charCodeAt(0) - 96);
  }
  return sum;
});
formulasInfo.push('I can sum the position in the alphabet of all the letters in the word.\n\n\t_stop_ = *70*\n\t_cab_ = *6*');
sequoiaNumber.push(87); // 19 + 5 + 17 + 21+ 15 + 9 + 1

// Convert word to binary: vowels are 0 and consonants are 1.
formulas.push(function(word) {
  var converted = '';
  for (letter of word) {
    if (VOWELS.includes(letter)) {
      converted += '0';
    } else {
      converted += '1';
    }
  }
  return parseInt(converted);
});
formulasInfo.push('I can convert the word to a binary number: vowels become *0*s and consonants become *1*s.\n\n\t_inertia_ = *0101100*\n\t_flint_ = *11011*');
sequoiaNumber.push(1010000);

// Current day of the month
formulas.push(function(word) {
  var today = new Date().getDate();
  return parseInt(today);
});
formulasInfo.push('I can _ignore_ the word and use the current *day of the month* (between *1* and *31*).\n\n\t_whatever_ = (current day of the month)\n\t_irrelevant_ = (current day of the month)');
sequoiaNumber.push(parseInt(new Date().getDate()));

// The biggest inner word
formulas.push(function(word) {
  var innerWord = '';
  for (aWord of WORDS) {
    if (word != aWord && word.includes(aWord)) {
      if (aWord.length > innerWord.length) {
        innerWord = aWord;
      }
    }
  }
  return innerWord.length;
});
formulasInfo.push('I can use the length of the largest word that is contained in the original word (zero if no word is found).\n\n\t_whatever_ = *4* (what, ever)\n\t_flag_ = *3* (lag)');
sequoiaNumber.push(3);

// Biggest alphabet distance between consecutive letters
formulas.push(function(word) {
  var lastPosition = word.charCodeAt(0) - 96;
  var biggestDistance = -1;
  for (var i = 1; i < word.length; i++) {
    var currentPosition = word.charCodeAt(i) - 96;
    var currentDistance = Math.abs(currentPosition - lastPosition);
    if (currentDistance > biggestDistance) {
      biggestDistance = currentDistance;
    }
    lastPosition = currentPosition;
  }
  return biggestDistance;
});
formulasInfo.push('I can use the biggest distance (considering the alphabet position) between two consecutive letters in the word.\n\n\t_ze_bra = *21*\n\t_ca_b = *2*');
sequoiaNumber.push(14);

// Shortest alphabet distance between consecutive letters
formulas.push(function(word) {
  var lastPosition = word.charCodeAt(0) - 96;
  var shortestDistance = 25;
  for (var i = 1; i < word.length; i++) {
    var currentPosition = word.charCodeAt(i) - 96;
    var currentDistance = Math.abs(currentPosition - lastPosition);
    if (currentDistance < shortestDistance) {
      shortestDistance = currentDistance;
    }
    lastPosition = currentPosition;
  }
  return shortestDistance;
});
formulasInfo.push('I can use the shortest distance (considering the alphabet position) between two consecutive letters in the word.\n\n\tf_ee_l = *0*\n\tc_ab_ = *1*');
sequoiaNumber.push(4);

// Alphabet distance between first and last letters
formulas.push(function(word) {
  var lastPosition = word.charCodeAt(0) - 96;
  var shortestDistance = 25;
  for (var i = 1; i < word.length; i++) {
    var currentPosition = word.charCodeAt(i) - 96;
    var currentDistance = Math.abs(currentPosition - lastPosition);
    if (currentDistance < shortestDistance) {
      shortestDistance = currentDistance;
    }
    lastPosition = currentPosition;
  }
  return Math.abs(word.charCodeAt(0) - word.charCodeAt(word.length - 1));
});
formulasInfo.push('I can use the distance (considering the alphabet position) between the first and the last letters of the word.\n\n\t_n_ylo_n_ = *0*\n\t_z_ebr_a_ = *25*');
sequoiaNumber.push(18);

// Consonant position (21)
for (let consonant of CONSONANTS) {
  formulas.push(function(v) {
    return function(word) {
      return word.indexOf(v) + 1;
    };
  }(consonant));
}
formulasInfo.push('I can use the position of the first letter *b* in the word.\n\n\t*b*_aby_ = *1*\n\t*b*_ack_ = *1*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *c* in the word.\n\n\t_ba_*c*_k_ = *3*\n\t*c*_ake_ = *1*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *d* in the word.\n\n\t_be_*d* = *3*\n\t_bir_*d* = *4*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *f* in the word.\n\n\t*f*_armer_ = *1*\n\t*f*_ather_ = *1*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *g* in the word.\n\n\t_do_*g* = *3*\n\t_e_*g*_g_ = *2*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *h* in the word.\n\n\t_birt_*h*_day_ = *5*\n\t_brot_*h*_er_ = *5*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *j* in the word.\n\n\t_ca_*j*_un_ = *3*\n\t*j*_upiter_ = *1*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *k* in the word.\n\n\t_bac_*k* = *4*\n\t_ca_*k*_e_ = *3*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *l* in the word.\n\n\t_app_*l*_e_ = *4*\n\t_ba_*l*_l_ = *3*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *m* in the word.\n\n\t_christ_*m*_as_ = *7*\n\t_far_*m*_er_ = *4*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *n* in the word.\n\n\t_chicke_*n* = *7*\n\t_childre_*n* = *8*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *p* in the word.\n\n\t_a_*p*_ple_ = *2*\n\t*p*_aper_ = *1*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *q* in the word.\n\n\t_s_*q*_uirrel_ = *2*\n\t*q*_ueen_ = *1*');
sequoiaNumber.push(3);
formulasInfo.push('I can use the position of the first letter *r* in the word.\n\n\t_bea_*r* = *4*\n\t_bi_*r*_d_ = *3*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *s* in the word.\n\n\t_chri_*s*_tmas_ = *5*\n\t_fi_*s*_h_ = *3*');
sequoiaNumber.push(1);
formulasInfo.push('I can use the position of the first letter *t* in the word.\n\n\t_bir_*t*_hday_ = *4*\n\t_boa_*t* = *4*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *v* in the word.\n\n\t_a_*v*_ocado_ = *2*\n\t*v*_acation_ = *1*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *w* in the word.\n\n\t_co_*w* = *3*\n\t_flo_*w*_er_ = *4*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *x* in the word.\n\n\t_bo_*x* = *3*\n\t_e_*x*_traordinary_ = *2*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *y* in the word.\n\n\t_bab_*y* = *4*\n\t_birthda_*y* = *8*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *z* in the word.\n\n\t*z*_ebra_ = *1*\n\t_ari_*z*_ona_ = *4*');
sequoiaNumber.push(0);

// Reverse consonant position (21)
for (let consonant of CONSONANTS) {
  formulas.push(function(v) {
    return function(word) {
      index = word.lastIndexOf(v);
      if (index < 0) {
        return 0;
      }
      return word.length - index;
    };
  }(consonant));
}
formulasInfo.push('I can use the position of the first letter *b* in the word, _counting from the end_.\n\n\t_ba_*b*_y_ = *2*\n\t*b*_ack_ = *4*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *c* in the word, _counting from the end_.\n\n\t_ba_*c*_k_ = *2*\n\t*c*_ake_ = *4*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *d* in the word, _counting from the end_.\n\n\t_be_*d* = *1*\n\t_bir_*d* = *1*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *f* in the word, _counting from the end_.\n\n\t*f*_armer_ = *6*\n\t*f*_ather_ = *6*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *g* in the word, _counting from the end_.\n\n\t_do_*g* = *1*\n\t_eg_*g* = *1*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *h* in the word, _counting from the end_.\n\n\t_birt_*h*_day_ = *4*\n\t_brot_*h*_er_ = *3*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *j* in the word, _counting from the end_.\n\n\t_ca_*j*_un_ = *3*\n\t*j*_upiter_ = *7*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *k* in the word, _counting from the end_.\n\n\t_bac_*k* = *1*\n\t_ca_*k*_e_ = *2*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *l* in the word, _counting from the end_.\n\n\t_app_*l*_e_ = *2*\n\t_bal_*l* = *1*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *m* in the word, _counting from the end_.\n\n\t_christ_*m*_as_ = *3*\n\t_far_*m*_er_ = *3*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *n* in the word, _counting from the end_.\n\n\t_chicke_*n* = *1*\n\t_childre_*n* = *1*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *p* in the word, _counting from the end_.\n\n\t_ap_*p*_le_ = *3*\n\t_pa_*p*_er_ = *3*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *q* in the word, _counting from the end_.\n\n\t_s_*q*_uirrel_ = *7*\n\t*q*_ueen_ = *5*');
sequoiaNumber.push(5);
formulasInfo.push('I can use the position of the first letter *r* in the word, _counting from the end_.\n\n\t_bea_*r* = *1*\n\t_bi_*r*_d_ = *2*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *s* in the word, _counting from the end_.\n\n\t_christma_*s* = *1*\n\t_fi_*s*_h_ = *2*');
sequoiaNumber.push(7);
formulasInfo.push('I can use the position of the first letter *t* in the word, _counting from the end_.\n\n\t_bir_*t*_hday_ = *5*\n\t_boa_*t* = *1*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *v* in the word, _counting from the end_.\n\n\t_a_*v*_ocado_ = *6*\n\t*v*_acation_ = *8*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *w* in the word, _counting from the end_.\n\n\t_co_*w* = *1*\n\t_flo_*w*_er_ = *3*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *x* in the word, _counting from the end_.\n\n\t_bo_*x* = *1*\n\t_e_*x*_traordinary_ = *12*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *y* in the word, _counting from the end_.\n\n\t_bab_*y* = *1*\n\t_birthda_*y* = *1*');
sequoiaNumber.push(0);
formulasInfo.push('I can use the position of the first letter *z* in the word, _counting from the end_.\n\n\t*z*_ebra_ = *5*\n\t_ari_*z*_ona_ = *4*');
sequoiaNumber.push(0);
