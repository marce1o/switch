const CONSONANTS = 'bcdfghjklmnpqrstvwxyz';

const WORDS = ['apple', 'baby', 'back', 'ball', 'bear', 'bed', 'bell', 'bird', 'birthday', 'boat', 'box', 'boy', 'bread', 'brother', 'cake', 'car', 'cat', 'chair', 'chicken', 'children', 'christmas', 'coat', 'corn', 'cow', 'day', 'dog', 'doll', 'door', 'duck', 'egg', 'eye', 'farmer', 'father', 'feet', 'fire', 'fish', 'floor', 'flower', 'game', 'garden', 'girl', 'good-bye', 'grass', 'ground', 'hand', 'head', 'hill', 'home', 'horse', 'house', 'kitty', 'leg', 'letter', 'man', 'men', 'milk', 'money', 'morning', 'mother', 'name', 'nest', 'night', 'paper', 'party', 'picture', 'pig', 'rabbit', 'rain', 'ring', 'robin', 'santa', 'school', 'seed', 'sheep', 'shoe', 'sister', 'snow', 'song', 'squirrel', 'stick', 'street', 'sun', 'table', 'thing', 'time', 'top', 'toy', 'tree', 'watch', 'water', 'way', 'wind', 'window', 'wood', 'queen', 'cajun', 'jupiter', 'avocado', 'vacation', 'extraordinary', 'zebra', 'arizona'];

const SEQUOIA = 'sequoia';

var output1 = '';
var output2 = '';

for (let consonant of CONSONANTS) {

  var pos1 = 0;
  var pos2 = 0;
  var word1 = 'bcs';
  var word2 = 'XXX';

  for (word of WORDS) {
    if (pos1 < 1) {
      var pos = word.indexOf(consonant) + 1;
      if (pos > 0) {
        pos1 = pos;
        word1 = word;
      }
    } else if (pos2 < 1) {
      var pos = word.indexOf(consonant) + 1;
      if (pos > 0) {
        pos2 = pos;
        word2 = word;
      }
    }
  }

  if (pos1 < 1) {
    throw 'Missing 2 words for ' + consonant;
  }

  if (pos2 < 1) {
    throw 'Missing 1 word for ' + consonant;
  }

  output1 += "formulasInfo.push('I can use the position of the first letter *" + consonant + "* in the word.\\n\\n\\t" + format(word1, consonant) + " = *" + pos1 + "*\\n\\t" + format(word2, consonant) + " = *" + pos2 + "*');\n";
  output1 += "sequoiaNumber.push(" + (SEQUOIA.indexOf(consonant) + 1) + ");\n";

  output2 += "formulasInfo.push('I can use the position of the first letter *" + consonant + "* in the word, _counting from the end_.\\n\\n\\t" + formatReverse(word1, consonant) + " = *" + indexOfReverse(word1, consonant) + "*\\n\\t" + formatReverse(word2, consonant) + " = *" + indexOfReverse(word2, consonant) + "*');\n";
  output2 += "sequoiaNumber.push(" + indexOfReverse(SEQUOIA, consonant) + ");\n";

}

function indexOfReverse(word, consonant) {
  return (word.length - word.lastIndexOf(consonant)) % (word.length + 1);
}

function format(word, consonant) {
  var formatted = '';
  var parts = word.replace(consonant, '@').split('@');
  if (parts[0] == '') {
    formatted = '*' + consonant + '*_' + parts[1] + '_';
  } else if (parts[1] == '') {
    formatted = '_' + parts[0] + '_*' + consonant + '*';
  } else {
    formatted = '_' + parts[0] + '_*' + consonant + '*_' + parts[1] + '_';
  }
  return formatted;
}

function formatReverse(word, consonant) {
  var formatted = '';
  var li = word.lastIndexOf(consonant);
  if (li == 0) {
    formatted = '*' + consonant + '*_' + word.substring(1) + '_';
  } else if (li == (word.length - 1)) {
    formatted = '_' + word.substring(0, word.length - 1) + '_*' + consonant + '*';
  } else {
    formatted = '_' + word.substring(0, li) + '_*' + consonant + '*_' + word.substring(li + 1) + '_';
  }
  return formatted;
}

// console.log(output1);
console.log(output2);

// fs.writeFile('consonants-formulas.txt', output1, (err) => {
//     if (err) throw err;
//     console.log('File saved!');
// });
