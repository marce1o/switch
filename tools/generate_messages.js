const readline = require('readline');
const fs = require('fs');

var data = {};

// Read from CSV file
const readInterface = readline.createInterface({
    input: fs.createReadStream('docs/messages.csv'),
    console: false
});

readInterface.on('line', function(line) {
    var firstComma = line.indexOf(',');
    var secondComma = line.indexOf(',', firstComma + 1);
    var typeValue = line.substring(0, firstComma);
    var ruleValue = line.substring(firstComma + 1, secondComma);

    var messageValue = line.substring(secondComma + 1).replace(/'/g, '’');
    if (messageValue[0] == '"') {
      messageValue = messageValue.substring(1, messageValue.length - 1);
    }

    // If there is a message,
    if (messageValue != '') {
      if (!data[typeValue]) {
        console.log('this.MSG_' + typeValue + ' = \'' + typeValue.toLowerCase() + '\';');
        data[typeValue] = [];
      }
      // group it by type
      data[typeValue].push({ruleValue, messageValue});
    }
    // console.log();
});

readInterface.on('close', function(line) {

  // For every type...
  var types = 0;
  for (var type in data) {
    if (data.hasOwnProperty(type)) {
      if (types == 0) {
        write('if(type == this.MSG_' + type + ') {');
      } else {
        write('else if(type == this.MSG_' + type + ') {');
      }
      var ifs = 0;
      for (var i = 0; i < data[type].length; i++) {
        var rule = data[type][i].ruleValue;
        var message = data[type][i].messageValue;

        if (ifs == 0) {
          if (rule != '') {
            write('  if(' + rule + ') {');
            write('    message = \'' + message + '\'');
            write('  }');
          } else {
            write('  message = \'' + message + '\'');
          }
        } else {
          if (rule != '') {
            write('  else if(' + rule + ') {');
            write('    message = \'' + message + '\'');
            write('  }');
          } else {
            write('  else {');
            write('    message = \'' + message + '\'');
            write('  }');
          }
        }
        ifs++;
      }
      write('}');
      types++;
    }
  }

});

function write(message) {
  console.log(message);
}
