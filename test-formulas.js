var fs = require('fs');

eval(fs.readFileSync('site/js/Switch.js') + ''); // Some common functions
eval(fs.readFileSync('site/js/Formulas.js') + '');
eval(fs.readFileSync('site/js/words.js') + '');

const WORD = 'sequoia';

console.log('* Number of available formulas', formulas.length);
console.log('* Number of available descriptions', formulasInfo.length);

if (formulas.length != formulasInfo.length) {
  throw 'Number of formulas is not equal to number of formula descriptions.';
}

if (formulas.length != sequoiaNumber.length) {
  throw 'Number of formulas is not equal to number of sequoia numbers.';
}

for (var i = 0; i < formulas.length; i++) {
  console.log('-----------------------');
  console.log(formulasInfo[i]);
  console.log();
  var number = formulas[i](WORD);
  console.log(WORD + ' = ' + number);
  if (number != sequoiaNumber[i]) {
    throw 'Formula result (' + number + ') is not the expected (' + sequoiaNumber[i] + ')';
  }
  var examples = formulasInfo[i].replace(/[*_]/g, '').match(/[a-z]+ = [0-9]+/g);
  console.log(examples);
  if (examples) {
    for (example of examples){
      var parts = example.split(/ = /);
      var exampleWord = parts[0];
      var exampleExpectedNumber = formulas[i](parts[0]);
      var exampleNumber = parts[1];
      if (exampleNumber != exampleExpectedNumber) {
        throw 'Example (' + example + ') do not match formula behavior (' + exampleWord + ' = ' + exampleExpectedNumber + ')';
      }
    }
  } else {
    console.log('NO EXAMPLES identified for this formula.');
  }

}
console.log('-----------------------');
console.log('SUCCESS');
console.log('-----------------------');
console.log('* Number of available formulas', formulas.length);
console.log('* Number of available descriptions', formulasInfo.length);
